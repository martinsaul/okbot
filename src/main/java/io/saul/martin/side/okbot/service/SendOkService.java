package io.saul.martin.side.okbot.service;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import io.saul.martin.side.okbot.helper.MessageHelper;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;


@Service
public class SendOkService {

    private final GmailService service;

    @Autowired
    public SendOkService(GmailService service) {
        this.service = service;
    }

    public boolean sendOk(Message lastMessage){
        try {
            MimeMessage message = createEmail(
                    MessageHelper.getHeader(lastMessage, "From"),
                    MessageHelper.getHeader(lastMessage, "Subject"),
                    MessageHelper.getHeader(lastMessage, "Message-ID"));
            sendMessage(service.serv(), "me", message);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    private MimeMessage createEmail(String to,
                                    String subject,
                                    String threadId)
            throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        if(threadId != null)
            email.setHeader("In-Reply-To", threadId);
        email.setFrom("me");
        email.addRecipient(javax.mail.Message.RecipientType.TO,
                new InternetAddress(to));
        email.setSubject(subject);
        email.setText("Ok.");
        return email;
    }

    private Message createMessageWithEmail(MimeMessage emailContent)
            throws MessagingException, IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailContent.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }

    public Message sendMessage(Gmail service,
                               String userId,
                               MimeMessage emailContent)
            throws MessagingException, IOException {
        Message message = createMessageWithEmail(emailContent);
        message = service.users().messages().send(userId, message).execute();

        System.out.println("Message id: " + message.getId());
        System.out.println(message.toPrettyString());
        return message;
    }
}
