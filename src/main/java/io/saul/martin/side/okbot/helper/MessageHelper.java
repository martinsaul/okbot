package io.saul.martin.side.okbot.helper;

import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;

import java.util.Optional;

public class MessageHelper {
    public static String getHeader(Message message, String field) {
        Optional<MessagePartHeader> targetHeader = message.getPayload().getHeaders().stream().filter(header -> header.getName().contains(field)).findFirst();
        return targetHeader.map(MessagePartHeader::getValue).orElse(null);
    }
}
