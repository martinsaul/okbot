package io.saul.martin.side.okbot.service;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.*;
import com.google.api.services.gmail.model.Thread;
import io.saul.martin.side.okbot.helper.MessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Service
public class SpamDetectionService {

    private final HashMap<String, String> knownThreads;
    private final GmailService service;
    private final SendOkService sendOkService;

    public SpamDetectionService(GmailService service, SendOkService sendOkService) {
        this.service = service;
        this.sendOkService = sendOkService;
        this.knownThreads = new HashMap<>();
    }

    @Scheduled(fixedRate = 10000)
    public void scan() throws IOException {
        String user = "me";
        ListLabelsResponse labelsResponse = service.exec().labels().list(user).execute();

        for(Label label: labelsResponse.getLabels()){
            if(label.getName().equalsIgnoreCase("SPAM")){
                ListThreadsResponse listResponse = service.exec().threads().list(user).setLabelIds(Collections.singletonList(label.getId())).execute();
                List<Thread> threads = listResponse.getThreads();
                if (threads == null || threads.isEmpty()) {
                    System.out.println("No SPAM found.");
                } else {
                    for (Thread shallowThread : threads) {
                        String threadId = shallowThread.getId();
                        Thread loadedThread = service.exec().threads().get(user, threadId).execute();
                        Message lastMessage = service.exec().messages().get(user, loadedThread.getMessages().get(loadedThread.getMessages().size() - 1).getId()).execute();
                        boolean fromMe = MessageHelper.getHeader(lastMessage, "From").contains(service.getMyEmail());
                        boolean knownLastMessage = knownThreads.containsKey(threadId) && knownThreads.get(threadId).equalsIgnoreCase(lastMessage.getId());
                        if(!knownLastMessage){
                            knownThreads.put(threadId, lastMessage.getId());
                            if(!fromMe) {
                                System.out.printf("- %s\n", shallowThread);
                                sendOkService.sendOk(lastMessage);
                            }
                        }
                    }
                }
            }
        }

    }
}
